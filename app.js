var express = require('express');
var passport = require('passport');
var logger = require('morgan');
var path = require('path');
// var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('connect-flash');

// tutaj dopisujemy kontrolery
var controllers = [
    'index',
    'paper',
    'conference',
    'user',
    'reports',
    'review',
    'charges'
];
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
// app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(
    session({secret: 'jsfkhsdkfdf', resave: true, saveUninitialized: false}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./config/passport')(passport);

app.use(function(req, res, next){
    res.locals.currentUser = req.user;
    next();
});

// konfiguracja routingu
controllers.forEach(function(file) {
  var router = require('./controllers/' + file)(passport);
  app.use(router);
});

// wyłapywanie błędów 404
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlery
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {message: err.message, error: err});
  });
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {message: err.message, error: {}});
});


module.exports = app;
