/**
 * Created by jacek on 2015-05-12.
 */

var express = require('express');
var paper = require('../models/paper');
var conference = require('../models/conference');
var participant = require('../models/participant');
var user = require('../models/user');

module.exports = function (passport) {
    var router = express.Router();

    router.get('/send_paper', function (req, res) {
        participant.selectRowByField({uzytkownik_id_uzytkownika: req.user.id_uzytkownika},
        function(err, row) {
            if(err)
                throw err;
            conference.getParticipantConferences(row.id_uczestnik, function(rows) {
                res.render('send_paper', {conferences: rows})
            });
        });
    });

    router.get('/paper/:id', function (req, res) {
        paper.selectRowByField({id_referat: req.params.id}, function (err, paper) {
            participant.selectRowByField({id_uczestnik: paper.uczestnik_id_uczestnik}, function(err, part) {
                user.selectRowByField({id_uzytkownika: part.uzytkownik_id_uzytkownika}, function(err, user) {
                    res.render('paper', {paper: paper, author: user});
                });
            });
            
        });
    });

    router.get('/list_paper', function (req, res, next) {
        res.render('list_paper')
    });

    router.get('/verify_paper', function (req, res, next) {
        res.render('verify_paper')
    });
    router.get('/assign_paper', function (req, res, next) {
        res.render('assign_paper')
    });

    router.get('/add_paper', function (req, res, next) {
        res.render('add_paper')
    });

    router.post('/paper', function (req, res) {
        var check = function (field) {
            return req.body.hasOwnProperty(field) && req.body[field].length > 0;
        };
        if (!check('nazwa'))
            res.render('send_paper', {error: "Nie podano nazwy referatu"});
        else {
                var obj = {
                    nazwa: req.body.nazwa,
                    konferencja_id_konferencja: parseInt(req.body.konferencje),
                    tresc: req.body.zawartosc,
                    sesja_id_sesja: req.body.id_sesji
                };
                paper.add(obj, function (id) {
                    res.redirect('/paper/' + id);
                });
        }
    });
    return router;
};
