/**
 * Created by Bartek on 2015-05-12.
 */
var express = require('express');
var conference = require('../models/conference');
var user = require('../models/user');
var participant = require('../models/participant');

var items = [
    {code: 'A1', name: 'Soda1', description: 'desc1'},
    {code: 'A2', name: 'Soda2', description: 'desc2'},
    {code: 'A3', name: 'Soda3', description: 'desc3'},
    {code: 'A4', name: 'Soda4', description: 'desc4'},
    {code: 'A5', name: 'Soda5', description: 'desc5'},
    {code: 'A6', name: 'Soda6', description: 'desc6'},
    {code: 'A7', name: 'Soda7', description: 'desc7'},
    {code: 'A8', name: 'Soda8', description: 'desc8'},
    {code: 'A9', name: 'Soda9', description: 'desc9'},
    {code: 'A10', name: 'Soda10', description: 'desc10'},
    {code: 'A11', name: 'Soda11', description: 'desc11'}
];

    module.exports = function (passport) {
    var router = express.Router();
    

    router.get('/conference/registration_for_participation', function(req, res, next) {
        conference.ShowConferences(function (conf) {
        res.render('conference/registration_for_participation', {conference: conf });   
        })

    });
    

    router.get('/conference/conference_add', function(req, res, next) {
        res.render('conference/conference_add', {items: items });
    
    });
    
    
     router.get('/conference/conference_edit', function(req, res, next) {
        res.render('conference/conference_edit', {items: items });
            
    
    });
       
        router.get('/conference/conference', function(req, res, next) {
        res.render('conference/conference', {items: items });

    });


     router.get('/conference_edit/:id', function (req, res) {
        conference.selectRowByField({id_konferencja: req.params.id}, function (err, conference) {
            console.log(conference);
            res.render('conference/conference_edit', {edited_conference: conference});
        });
    });

    router.post('/conference_add', function(req, res, next) {
     
     var nowy={
         
         nazwa : req.body.nazwa,
         adres : req.body.adres,
         data_rozpoczecia : req.body.datroz,
         data_zakonczenia : req.body.datzak,
         aktywna : req.body.optionAktywna,
         koszty :  req.body.koszty
     
       };
       
       console.log(nowy);
       
       conference.create(nowy, function (id){
           
         res.render('conference/conference_add', {message: "Dodano konferencje o id: "+ id});  
       });
       });
       
    router.post('/conference_edit/:id', function(req, res, next) {
     
     var nowy={
         
         nazwa : req.body.nazwa,
         adres : req.body.adres,
         data_rozpoczecia : req.body.datroz,
         data_zakonczenia : req.body.datzak,
         aktywna : req.body.optionAktywna === '1',
         koszty :  req.body.koszty
     
       };
       
       console.log(nowy);
       
      conference.updateRowByField({id_konferencja: req.params.id}, nowy, function () {
            res.redirect('/conference/conference_info')
       });
       });
       
        router.get('/conference/conference_info', function(req, res, next) {
            conference.ShowConferences(function (conf1) {
            res.render('conference/conference_info', { conference: conf1 });
            });
        });
     
    
  
      router.get('/conference/registration_selected/:id', function(req, res, next) {
         conference.selectRowByField({id_konferencja: req.params.id}, function (err, conference) {
            console.log(conference);
            res.render('conference/registration_selected', {selected_conference: conference});
        });
            
    
    });
    
    router.post('/conference/registration_selected/:id', function (req, res) {
        
        var uczestnik={

            zakwaterowanie: false,
            referat: false,
            //hotel_id_hotel: 0,
            uzytkownik_id_uzytkownika: req.user.id_uzytkownika
            
        };
        if(req.body.optionAktywna === '1')
        { 
         uczestnik.zakwaterowanie = true;
         uczestnik.hotel_id_hotel = 0;  
        }
        
        participant.registerForUser(uczestnik, function (id_uczest){
            var nowy={
                uczestnik_id_uczestnik: id_uczest,
                konferencja_id_konferencja: req.params.id
            };
            conference.AddIntoConference(nowy, function (id){
                res.redirect('/conference/conference_participants_list');
            });
        });
    });
        router.get('/conference/conference_participants_list', function(req, res, next) {
            conference.ShowConferences(function (conf1) {
            res.render('conference/conference_participants_list', { conference: conf1 });
            });
        });
         router.get('/conference_participants/:id', function (req, res,next) {
        conference.ShowParticipants({id_konferencja: req.params.id}, function (users) {
            console.log(users);
            res.render('conference/conference_participants', {uzytkownik: users});
        });
    });

    return router;
    
   
};
