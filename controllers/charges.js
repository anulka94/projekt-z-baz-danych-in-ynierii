/**
 * Created by Mateusz on 2015-05-25.
 */
var express = require('express');
var charges = require('../models/charges');

module.exports = function (passport) {
    var router = express.Router();

    router.get('/charges/charges', function(req, res, next) {
        res.render('charges/charges', { title: 'Przegląd opłat' });
    });
    
    router.get('/charges/choose_conference', function(req, res, next) {
        res.render('charges/choose_conference', { title: 'Wprowadzenie kosztów' });
    });
    
    router.get('/charges/conference_costs', function(req, res, next) {
        charges.ShowConferences(function (conf) {
        res.render('charges/conference_costs', { conference: conf });
        });
    });
    
    router.get('/charges/review_conference', function(req, res, next) {
        res.render('charges/review_conference', { title: 'Przegląd' });
    });
    
    router.get('/charges/review_costs', function(req, res, next) {
        res.render('charges/review_costs', { title: 'Przegląd' });
    });
    
    router.get('/charges/payment_confirm', function(req, res, next) {
        res.render('charges/payment_confirm', { title: 'Potwierdzenie wplaty' });
    });
    
    return router;
};