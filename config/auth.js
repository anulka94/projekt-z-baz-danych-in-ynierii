/**
 * Created by jacek on 27.05.15.
 */

module.exports = {
    isUser: function(req, res, next) {
        if(req.isAuthenticated())
            next();
        else
            res.redirect('/login');
    },
    isAdmin: function(req, res, next) {
        if(req.isAuthenticated() && req.user.adm)
            next();
        else
            res.redirect('/login');
    },
    isParticipant: function(req, res, next) {
        if(req.isAuthenticated() && req.user.uczestnik)
            next();
        else
            res.redirect('/login');
    },
    isReviewer: function(req, res, next) {
        if(req.isAuthenticated() && req.user.recenzent)
            next();
        else
            res.redirect('/login');
    }
};