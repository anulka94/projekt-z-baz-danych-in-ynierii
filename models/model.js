var ErrorCode = Object.freeze({
    NOT_FOUND: 0,
    INVALID_PASSWORD: 1
});

module.exports.ErrorCode = ErrorCode;

module.exports.Model = {
    connection: require('../config/db'),
    selectMaxInColumn: function (column, callback) {
        this.connection.query(
            'SELECT MAX(??) FROM ??',
            [column, this.table], function (err, rows, fields) {
                var query = 'MAX(`' + column + '`)';
                if (err)
                    throw err;
                else if (rows.length === 0 || rows[0][query] === null)
                    callback(ErrorCode.NOT_FOUND, null);
                else
                    callback(null, rows.pop()[query]);
            });
    }, insert: function (row, callback) {
        this.connection.query('INSERT INTO ?? SET ?',
            [this.table, row], function (err, result) {
                if (err)
                    throw err;
                else
                    callback(result);
            });
    }, selectRowByField: function (field, callback) {
        var key = Object.keys(field).pop();
        this.connection.query('SELECT * FROM ?? WHERE ?? = ?',
            [this.table, key, field[key]], function (err, rows, fields) {
                if (err) throw err;
                else if (rows.length === 0)
                    callback(ErrorCode.NOT_FOUND, null);
                else
                    callback(null, rows.pop());
            });
    }, selectAll: function (callback) {
        this.connection.query('SELECT * FROM ??', [this.table], function (err, rows, fields) {
            if (err)
                throw err;
            else
                callback(rows);
        });
    }, updateRowByField: function (field, value, callback) {
        var key = Object.keys(field).pop();
        this.connection.query('UPDATE ?? SET ? WHERE ?? = ?',
            [this.table, value, key, field[key]], function (err, result) {
                if (err)
                    throw err;
                else
                    callback();
            });
    }, 
};