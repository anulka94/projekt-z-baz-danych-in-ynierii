/**
 * Created by jacek on 2015-05-03.
 */


    var Model = require('./model').Model;
    var ErrorCode = require('./model').ErrorCode;

    
    module.exports = Object.create(Model, {
    table: {value: 'konferencja'},
    //miejsce na funkcje
    create:{
        value: function (conference, callback) {
            var that = this;
            this.selectMaxInColumn('id_konferencja', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    conference.id_konferencja = 0;
                else
                    conference.id_konferencja = max + 1;
                that.insert(conference, function (result) {
                    callback(conference.id_konferencja);
                                });
            });
        }
    },
    ShowConferences:{
    value : function(callback){
        this.connection.query('select k.id_konferencja as id_konferencja, k.nazwa as nazwa, k.data_rozpoczecia as data_rozpoczecia, k.data_zakonczenia as data_zakonczenia, k.adres as adres from konferencja k',
        function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    
            CreateCalendar: 
    {
        value:function(callback){
            this.connection.query('select u.imie as imie, u.nazwisko as nazwisko, ref.nazwa as referat,'
            +' k.nazwa as konferencja, s.nazwa as sesja, k.data_rozpoczecia as data from uzytkownik u,'
            +' przypisanie p, referat ref, konferencja k, sesja s where u.id_uzytkownika=p.uzytkownik_id_uzytkownika' 
            +' and p.konferencja_id_konferencja=k.id_konferencja and k.sesja_id_sesja=s.id_sesja and' 
            +' p.id_przypisanie=ref.przypisanie_id_przypisanie and u.uczestnik=1 order by s.nazwa, k.nazwa', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
        },
        
    ShowActiveConferences:{
    value : function(callback){
        this.connection.query('select k.nazwa as nazwa, k.adres as adres from konferencja k where k.aktywna=1 ',
        function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    ShowParticipants:{
     value : function(conference,callback){
         var key = conference;
         this.connection.query('select p.imie as imie,p.nazwisko as nazwisko,p.email as email from uzytkownik p,przypisanie przyp, konferencja k,uczestnik u where u.uzytkownik_id_uzytkownika=p.id_uzytkownika and przyp.uczestnik_id_uczestnik=u.id_uczestnik and przyp.konferencja_id_konferencja=k.id_konferencja and k.id_konferencja=?',[conference.id_konferencja],
         function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
     }
    },
    AddIntoConference:{
        value : function(obj,callback){
            this.connection.query('insert into przypisanie set ?', [obj], function(err, result) {
                callback();
            })
        }
    },
    getParticipantConferences: {
        value: function(part_id, callback) {
            this.connection.query('select * from przypisanie p, konferencja k where p.uczestnik_id_uczestnik = ? ' + 
                                'and k.id_konferencja = p.konferencja_id_konferencja'
            , [part_id],
            function(err, rows, fields) {
                if(err)
                    throw err;
                callback(rows);
            })   
        }
    }
    });


