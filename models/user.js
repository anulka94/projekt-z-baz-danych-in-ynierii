/**
 * Created by jacek on 2015-05-03.
 */

var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;
var bcrypt = require('bcrypt');

module.exports = Object.create(Model, {
    //nazwa tabeli
    table: {value: 'uzytkownik'},
    verifyCredentials: {
        value: function (email, password, callback) {
            this.selectRowByField({email: email}, function (err, user) {
                if (err === ErrorCode.NOT_FOUND)
                    callback(err, null);
                else {
                    bcrypt.compare(
                        password, user.haslo, function (err, res) {
                            if (res)
                                callback(null, user);
                            else
                                callback(ErrorCode.INVALID_PASSWORD, null);
                        });
                }
            });
        }
    },
    
    create: {
        value: function (user, callback) {
            var that = this;
            this.selectMaxInColumn('id_uzytkownika', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    user.id_uzytkownika = 0;
                else
                    user.id_uzytkownika = max + 1;
                bcrypt.genSalt(
                    10, function (err, salt) {
                        bcrypt.hash(
                            user.haslo, salt, function (err, hash) {
                                user.haslo = hash;
                                that.insert(user, function (result) {
                                    callback(user.id_uzytkownika);
                                });
                            });
                    });
            });
        }
    },
    
    SelectAllParticipants: 
    {
        value:function(callback){
            this.connection.query('select u.imie as imie, u.nazwisko as nazwisko, u.email as email, s.nazwa as sesja, k.nazwa as'
            + ' konferencja, r.nazwa as referat, u.aktywny as aktywny from uzytkownik u, sesja s, konferencja k, referat r, przypisanie p'
            + ' where (u.id_uzytkownika=p.uzytkownik_id_uzytkownika and p.konferencja_id_konferencja=k.id_konferencja and k.sesja_id_sesja'
            + ' =s.id_sesja) and (u.id_uzytkownika=p.uzytkownik_id_uzytkownika and p.id_przypisanie=r.przypisanie_id_przypisanie)'
            + ' and u.uczestnik=1 order by s.nazwa', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    },
    
        SelectAllAdmins: 
    {
        value:function(callback){
            this.connection.query('select imie, nazwisko, adres, firma, email from uzytkownik where adm=1', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    },
    
        SelectAllReviewers: 
    {
        value:function(callback){
            this.connection.query('select u.imie as imie, u.nazwisko as nazwisko, u.email as email,'
            + ' u.aktywny as aktywny, r.nazwa as referat, k.nazwa as konferencja, s.nazwa as sesja from uzytkownik u,'
            + ' przypisanie p, referat r, p_recenzenta pr, konferencja k, sesja s where u.id_uzytkownika=pr.uzytkownik_id_uzytkownika'
            + ' and pr.referat_id_referat=r.id_referat and p.id_przypisanie=r.przypisanie_id_przypisanie and p.konferencja_id_konferencja='
            + ' k.id_konferencja and k.sesja_id_sesja=s.id_sesja and recenzent=1 order by s.nazwa', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    },
        
        SelectAllLoad: 
    {
        value:function(callback){
            this.connection.query('select u.imie, u.nazwisko, count(p.uzytkownik_id_uzytkownika) as referaty,'
            + ' count(r.p_recenzenta_id_przypisania) as recenzje from uzytkownik u, p_recenzenta p, recenzja r'
            +' where u.id_uzytkownika=p.uzytkownik_id_uzytkownika and r.p_recenzenta_id_przypisania=p.id_przypisania'
            +' and u.recenzent=1 group by u.nazwisko, u.imie', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    }, 
    
    selectUser: {
        value: function (id, callback) {
        this.connection.query('SELECT * FROM ?? WHERE id_uzytkownika = ?',
            [this.table, id], function (err, rows, fields) {
                if (err) throw err;
                else if (rows.length === 0)
                    callback(ErrorCode.NOT_FOUND, null);
                else
                    callback(null, rows[0]);
            }
            );
        }
    },
    
    updateUser: {
        value: function (id, user, callback) {
        this.connection.query('UPDATE ?? SET ? WHERE id_uzytkownika = ?',
            [this.table , user, id], function (err, result) {
                if (err) throw err;
                else
                callback();
            }
            );
        }
    },
    
});