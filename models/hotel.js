/**
 * Created by jacek on 27.05.15.
 */
var Model = require('./model').Model;

module.exports = Object.create(Model, {
    table: {value: 'hotel'},
    
     SelectAllAccommodation: 
    {
        value:function(callback){
            this.connection.query('select k.data_rozpoczecia as datar, k.data_zakonczenia as dataz,'
            + ' u.imie as imie, u.nazwisko as nazwisko, u.oplaty as oplaty, k.nazwa as konferencja,'
            + ' h.nazwa as nazwah, o.kwota as oplata_calkowita, (o.kwota-u.oplaty) as doplata,'
            + ' h.adres as adres from uzytkownik u, przypisanie p, konferencja k, oplaty o, hotel h'  
            + ' where u.id_uzytkownika=p.uzytkownik_id_uzytkownika'
            + ' and p.hotel_id_hotel=h.id_hotel and p.konferencja_id_konferencja=k.id_konferencja'
            + ' and k.id_konferencja=o.konferencja_id_konferencja and ((u.uczestnik=1 and recenzent=0)'
            + ' or (u.uczestnik=0 and recenzent=1)) and (k.data_zakonczenia-k.data_rozpoczecia)>0 order'
            + ' by k.data_rozpoczecia, u.nazwisko, u.imie', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    }
});
