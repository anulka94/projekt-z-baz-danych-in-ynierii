/**
 * Created by jacek on 27.05.15.
 */

var Model = require('./model').Model;

module.exports = Object.create(Model, {
    table: {value: 'recenzja'},
    
    
     SelectAllReviews: 
    {
        value:function(callback){
            this.connection.query('select r.nazwa as recenzja, u.imie as imie, u.nazwisko as nazwisko,'
            +' ref.nazwa as referat, k.nazwa as konferencja, s.nazwa as sesja from referat ref, p_recenzenta pr,'
            +' uzytkownik u, recenzja r, konferencja k, sesja s, przypisanie p where ref.id_referat='
            +' pr.referat_id_referat and u.id_uzytkownika=pr.uzytkownik_id_uzytkownika and r.p_recenzenta_id_przypisania='
            +' pr.id_przypisania and r.zatwierdzona=0 and ref.przypisanie_id_przypisanie=p.id_przypisanie and'
            +' p.konferencja_id_konferencja=k.id_konferencja and k.sesja_id_sesja=s.id_sesja order by s.nazwa,'
            +' k.nazwa, u.nazwisko, u.imie, ref.nazwa', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
        });
      },
        
    getAllForReviewer: {
        value: function(user_id, callback) {
            this.connection.query('select r.id_recenzja, r.nazwa, count(k.data) from recenzja r left join ' +
                                'komentarz k on k.recenzja_id_recenzja = r.id_recenzja where ' +
                                'r.uzytkownik_id_uzytkownika = ? and r.skonczona = true ' +
                                'group by r.id_recenzja',
            [user_id], function(err, rows, fields) {
                if(err)
                    throw err;
                callback(rows);
            });
        }
    }
}
});
