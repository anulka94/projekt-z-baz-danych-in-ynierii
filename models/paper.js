/**
 * Created by jacek on 2015-05-12.
 */
 
var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;
 
 
module.exports = Object.create(Model, {
    table: {value: 'referat'},
    add: {
        value: function (paper, callback) {
            var that = this;
            this.selectMaxInColumn('id_referat', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    paper.id_referat = 0;
                else
                    paper.id_referat = max + 1;
                that.insert(paper, function (result) {
                    callback(paper.id_referat);
                });
            });
        }
    },
    
    SelectAllArticles: 
    {
        value:function(callback){
            this.connection.query('select s.nazwa as sesja, k.nazwa as konferencja, u1.imie as imie,'
            + ' u1.nazwisko as nazwisko, r.nazwa as referat, o.ocena_zgodnosci ocenaz, o.ocena_oryginalnosci'
            + ' as ocenao, o.ocena_jakosci as ocenaj, o.ocena_poprawnosci as ocenap, (o.ocena_zgodnosci+'
            + ' o.ocena_oryginalnosci+o.ocena_jakosci+o.ocena_poprawnosci)/4 as ocena_sr, u2.imie as imie2,' 
            + ' u2.nazwisko as nazwisko2 from uzytkownik u1, przypisanie p, referat r, p_recenzenta pr, recenzja o,'
            + ' konferencja k, sesja s, uzytkownik u2 where (u1.id_uzytkownika=p.uzytkownik_id_uzytkownika and '
            + ' k.id_konferencja=p.konferencja_id_konferencja and k.sesja_id_sesja=s.id_sesja and p.id_przypisanie='
            + ' r.przypisanie_id_przypisanie and r.id_referat=pr.referat_id_referat and o.p_recenzenta_id_przypisania=pr.id_przypisania'
            + ' and u1.uczestnik=1) and (u2.id_uzytkownika=pr.uzytkownik_id_uzytkownika and pr.id_przypisania=o.p_recenzenta_id_przypisania'
            + ' and pr.referat_id_referat=r.id_referat and u2.recenzent=1) order by s.nazwa, k.nazwa, u1.nazwisko, u2.imie', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    },
    getAllAssignedToReviewer: {
        value: function(user_id, callback) {
            this.connection.query('SELECT rf.id_referat, rf.nazwa FROM recenzja rc, referat rf' + 
                                ' WHERE rc.uzytkownik_id_uzytkownika = ?' +
                                ' AND rc.skonczona=FALSE AND rc.referat_id_referat=rf.id_referat',
                                [user_id], function(err, rows, fields) {
                                    if(err)
                                        throw err;
                                    callback(rows);
                                });
        }
    }
});