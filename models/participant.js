/**
 * Created by jacek on 27.05.15.
 */

var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;

module.exports = Object.create(Model, {
    table: {value: 'uczestnik'},
    registerForUser: {
        value: function(obj, callback) {
            var that = this
            this.selectMaxInColumn('id_uczestnik', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    obj.id_uczestnik = 0;
                else
                    obj.id_uczestnik = max + 1;
                that.insert(obj, function() {
                    callback(obj.id_uczestnik);
                }) 
            });
        }
    }
});
