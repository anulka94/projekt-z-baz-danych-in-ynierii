### Instalacja ###

W konsoli programu git:

* wejdź do folderu, w którym chcesz umieścić projekt
* git clone https://jac3km4@bitbucket.org/jac3km4/projekt-z-baz-danych-in-ynierii.git
* cd projekt-z-baz-danych-in-ynierii
* npm install


### Synchronizacja ###

* git fetch upstream
* git checkout master
* git merge upstream/master