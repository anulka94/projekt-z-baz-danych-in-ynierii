
var msce = [
    'styczeń',
    'luty',
    'marzec',
    'kwiecień',
    'maj',
    'czerwiec',
    'lipiec',
    'sierpień',
    'wrzesień',
    'październik',
    'listopad',
    'grudzień'
    ];
var dni = [
    'pon',
    'wt',
    'śr',
    'czw',
    'pt',
    'so',
    'ni'
    ];

module.exports = {
    format: function(date) {
        return dni[date.getDay()] + ', ' + date.getDate() + ' ' + msce[date.getMonth()] + ' ' + date.getFullYear();
    }
}