

module.exports = {
    checkForField: function(name, req) {
        return req.body.hasOwnProperty(name) && req.body[name].length > 0
    }
}